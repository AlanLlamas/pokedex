

var
//estas variables crean a los pokemon, se le dan valos de 1.-Nombre 2.-Ataque 3.-Tipo 4.-Defensa 5.-Ataque1 6.- Ataque2 7.-Ataque3 .
	bulbasaur  = new estructura("Bulbasaur",50, "Hierba / Veneno", 30, "Hoja Navaja: 20", "Latigo Cepa: 15","Tacleo: 10" ),
	charmander = new estructura("Charmander", 60, "Fuego", 20, "Lanzallamas: 25", "Bola de Fuego: 20","Tacleo: 10"),
	squirtle = new estructura("Squirtle", 40, "Agua", 40, "Chorro de Agua: 15", "Hidrobomba: 15","Tacleo: 10"),
	Caterpie = new estructura("Caterpie", 30, "Bicho", 30, "Hilo de Seda: 10", "Capullo de Seda : 10","Tacleo: 10"),
	Weedle = new estructura("Weedle",35,"Bicho / Veneno", 25,"Aguijon Venenoso: 15","Capullo de Seda","Tacleo: 10"),
	Pidgey = new estructura("Pidgey", 30,"Normal / Volador",35,"Picotazo: 15","Aleteo: 10","Tacleo: 10"),
	Rattata = new estructura("Rattata", 30,"Normal", 30,"Mordida : 15","Chillido: 10","Tacleo: 10"),
	Spearow = new estructura("Spearow", 35,"Normal / Volador",30,"Picotazo: 15","Aleteo: 10","Tacleo: 10"),
	Ekans = new estructura("Ekans", 40,"Veneno", 35,"Mordida Venenosa: 20","Estrangulamiento: 15","Tacleo: 10"),
	Pikachu = new estructura("Pikachu", 50,"Elèctrico", 30,"Impact Trueno: 25","Relàmpago: 15","Tacleo: 10"),
	Sandshrew = new estructura("Sandshrew", 40,"Tierra", 50,"Velo de Arena: 20","Cavar: 15","Tacleo: 10"),
	NidoranH = new estructura("Nidoran Hembra", 35,"Veneno", 35,"Cornada Venenosa: 15","Mordida: 10","Tacleo: 10"),
	NidoranM = new estructura("Nidoran Macho", 40,"Veneno", 35,"Cornada Venenosa: 20","Mordida: 15","Tacleo: 10"),
	Clefairy = new estructura("Clefairy", 35,"Hada", 40,"Encanto Misterioso: 20","Muro Magico: 15","Tacleo: 10"),
	Vulpix = new estructura("Vulpix", 45,"Fuego", 35,"Lanzallamas: 25","Bola de Fuego: 20","Tacleo: 10"),
	Jigglypuff = new estructura("Jigglypuff",30,"Normal / Hada", 45,"Canciòn: 20","Encanto Misterioso: 15","Tacleo: 10");
	Zubat = new estructura("Zubat", 40,"Veneno / Volador", 35,"Supersonico: 20","Chupasangre: 15","Tacleo: 10"),
	Oddish = new estructura("Oddish", 45,"Hierba / Veneno", 40,"Absorber Energìa: 25","Polvos Venenosos: 15","Tacleo: 10"),
	Paras = new estructura("Paras", 50,"Insecto / Hierba", 40,"Efecto Espora: 20","Hongos Venenosos: 15","Tacleo: 10"),
	Venonat = new estructura("Venonat", 55,"Insecto / Veneno", 50,"Recuperacion: 25","Espora Venenosa: 15","Tacleo: 10");
//Aqui llamo a la funcion pokemon, la cual le pasa el objeto a usar como argumento y le agrega el evento click  a la lista.
pokemon(bulbasaur);
pokemon(charmander);
pokemon(squirtle);
pokemon(Caterpie)
pokemon(Weedle);
pokemon(Pidgey);
pokemon(Rattata);
pokemon(Spearow);
pokemon(Ekans);
pokemon(Pikachu);
pokemon(Sandshrew);
pokemon(NidoranH);
pokemon(NidoranM);
pokemon(Clefairy);
pokemon(Vulpix);
pokemon(Jigglypuff);
pokemon(Zubat);
pokemon(Oddish);
pokemon(Paras);
pokemon(Venonat);

function imprimir (arg) {
//esta funcion pone la imagen y la informacion del pokemon.

	var
		info = document.getElementById('informacion'),
		img = document.getElementById('pokemon'),
		infoInput = "<h2>"+arg.nombre+"</h2> <p>Ataque = "+arg.ataque+"</p> <p>Defensa = "+arg.defensa+"</p> <p>Tipo = "+arg.tipo+"</p> <p>Vida = "+arg.vida+"</p> <p>Ataques: <br><br>    -"+arg.ataques.ataque1+"<br> <br>    -"+arg.ataques.ataque2+"<br> <br>    -"+arg.ataques.ataque3+"</p>";
		info.innerHTML = infoInput;
		img.src =  "img/" + arg.nombre +".jpg";
}



function pokemon (arg) {
//esta funcion le agrega el evento click a la lista de pokemones y al darle click llama la funcion imprimir linea 7
	
			pokes = document.getElementById(arg.nombre);
			pokes.addEventListener('click', function () {
				imprimir(arg);
				console.log(arg.nombre);	
			})
};


function estructura (n,a,t,d,a1,a2,a3) {
//esta funcion crea la estructura para ingresar los datos de los pokemon.
	this.nombre = n,
	this.vida = 100,
	this.ataque = a,
	this.tipo = t,
	this.defensa = d,
	this.ataques = {

		ataque1 : a1,
		ataque2 : a2,
		ataque3 : a3
	}
	return this;
}
